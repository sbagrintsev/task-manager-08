package ru.tsc.bagrintsev.tm.constant;

public final class CommandLineConst {

    public static final String VERSION_SHORT = "-v";

    public static final String ABOUT_SHORT = "-a";

    public static final String HELP_SHORT = "-h";

    public static final String INFO_SHORT = "-i";

    public static final String ARGUMENTS_SHORT = "-arg";

    public static final String COMMANDS_SHORT = "-cmd";

}
